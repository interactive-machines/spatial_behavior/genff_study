cmake_minimum_required(VERSION 2.8.3)
project(publish_data_to_unity)

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

find_package(catkin REQUIRED COMPONENTS
    tf
    message_generation
    std_msgs
    geometry_msgs
)

add_message_files(
  FILES
  TrackedPerson.msg
  TrackedPersons.msg
)

generate_messages(
  DEPENDENCIES
    std_msgs
    geometry_msgs
)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES ${PROJECT_NAME}
  CATKIN_DEPENDS
    std_msgs
    geometry_msgs
)
