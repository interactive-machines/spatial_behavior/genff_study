for i in fake/*.csv; do
    name=`basename ${i} .csv`
    echo "Generating ${name}..."
    rosrun publish_data_to_unity publish_csv_positions.py _csv:=${i} _root_output:=recordings/${name} _camera:=low
done
