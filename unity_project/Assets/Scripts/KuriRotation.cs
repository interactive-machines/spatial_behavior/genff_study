﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class KuriRotation : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    public float forceMultiplier = 0.4f;
    public float goalFactor = 1;
    public float agentFactor = 1;
    public float wallFactor = 1;

    public float groundCheckDistance = 0.3f;
    public float perceptionRadius = 1.5f;
    private GameObject robot;
    private GameObject robotObject;
    private GameObject virtualColliderObject;

    // Update is called once per frame
    void Update()
    {
        robot = Resources.Load<GameObject>("Kuri");
       	robotObject = Instantiate(robot, transform.position, transform.rotation);
        robotObject.transform.parent = transform;
        robotObject.tag = "Actor";

        Rigidbody rb = robotObject.GetComponent<Rigidbody>();

        rb.inertiaTensor = new Vector3(0.01f, 0.01f, 0.01f);
        rb.centerOfMass = Vector3.zero;
        rb.constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationX;

        virtualColliderObject = new GameObject();
        virtualColliderObject.transform.position = transform.position;
        virtualColliderObject.transform.rotation = transform.rotation;
        virtualColliderObject.transform.parent = robotObject.transform;
        virtualColliderObject.transform.SetSiblingIndex(0);
        virtualColliderObject.layer = 2;    // Ignore Raycast layer
        virtualColliderObject.name = "SocialForcesCollider";
        var sphereComp = virtualColliderObject.AddComponent<SphereCollider>();
        sphereComp.isTrigger = true;
        sphereComp.radius = perceptionRadius;

        var thirdComp = robotObject.AddComponent<ThirdPersonCharacter>();
        thirdComp.m_GroundCheckDistance = groundCheckDistance;

        /*var agentComp = robotObject.AddComponent<Agent>();
        agentComp.enabled = false;
        agentComp.FORCE_MULTIPLIER = forceMultiplier;
        agentComp.GOAL_FACTOR = goalFactor;
        agentComp.AGENT_FACTOR = agentFactor;
        agentComp.WALL_FACTOR = wallFactor;

        var navComp = robotObject.AddComponent<NavMeshAgent>();
        navComp.enabled = false;
        navComp.baseOffset = 1;
        navComp.speed = 3.5f;
        navComp.angularSpeed = 120;
        navComp.acceleration = 8;
        navComp.stoppingDistance = 0;
        navComp.autoBraking = true;
        navComp.radius = 1;
        navComp.height = 2; */
    }
}
