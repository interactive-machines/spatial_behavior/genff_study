﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace RosSharp.RosBridgeClient
{

    public class SingleCameraPublisher : UnityPublisher<MessageTypes.Sensor.CompressedImage>
    {
    
        public Camera ImageCamera;
        public string FrameId = "Camera";
        public int resolutionWidth = 640;
        [Range(0, 100)]
        public int qualityLevel = 50;

        private MessageTypes.Sensor.CompressedImage message;
        private Texture2D texture2D;
        private Rect rect;

        protected override void Start()
        {
	    Debug.Log("Screen width:" + Screen.width + " height: " + Screen.height);
            base.Start();
            InitializeGameObject();
            InitializeMessage();
            Camera.onPostRender += UpdateImage;
        }

        private void UpdateImage(Camera _camera)
        {
            if (texture2D != null && _camera == this.ImageCamera)
                UpdateMessage();
        }

        private void InitializeGameObject()
        {
	    int h = resolutionWidth*Screen.height/Screen.width;
            texture2D = new Texture2D(resolutionWidth, h, TextureFormat.RGB24, false);
	    Debug.Log("Texture 2D width:" + texture2D.width + " height: " + texture2D.height);
            rect = new Rect(0, 0, resolutionWidth, h);
 	    //We omit the line below or the camera won't render
            //ImageCamera.targetTexture = new RenderTexture(resolutionWidth, h, 24);
        }

        private void InitializeMessage()
        {
            message = new MessageTypes.Sensor.CompressedImage();
            message.header.frame_id = FrameId;
            message.format = "jpeg";
        }

        private void UpdateMessage()
        {

	    RenderTexture transformedRenderTexture = null;
            RenderTexture renderTexture = RenderTexture.GetTemporary(
                Screen.width,
                Screen.height,
                24,
                RenderTextureFormat.ARGB32,
                RenderTextureReadWrite.Default,
                1);

	    try
            {
		
		ScreenCapture.CaptureScreenshotIntoRenderTexture(renderTexture);
                transformedRenderTexture = RenderTexture.GetTemporary(
                    texture2D.width,
                    texture2D.height,
                    24,
                    RenderTextureFormat.ARGB32,
                    RenderTextureReadWrite.Default,
                    8);
                Graphics.Blit(
                    renderTexture,
                    transformedRenderTexture,
                    new Vector2(1.0f, 1.0f),
                    new Vector2(0.0f, 0.0f));
                RenderTexture.active = transformedRenderTexture;
                texture2D.ReadPixels(
                    rect,
                    0, 0);
            }
            catch (Exception e)
            {
                Debug.Log("Exception: " + e);
                
            }
	    finally
            {
                RenderTexture.active = null;
                RenderTexture.ReleaseTemporary(renderTexture);
                if (transformedRenderTexture != null)
                {
                    RenderTexture.ReleaseTemporary(transformedRenderTexture);
                }
            }
 
            texture2D.Apply();

            message.header.Update();
            message.data = texture2D.EncodeToJPG(qualityLevel);
            Publish(message);
        }
    }

}
